"use strict";

var gulp = require("gulp");
var sass = require("gulp-sass");
var browserSync = require("browser-sync").create();
const autoprefixer = require('gulp-autoprefixer');

sass.compiler = require("node-sass");

gulp.task("sass", function () {
    return gulp
        .src("./src/sass/**/*.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(gulp.dest("./src/css"))
        .pipe(browserSync.stream());
});

function reload(done) {
    browserSync.reload();
    done();
};

gulp.task("sass:watch", function () {
    browserSync.init({
        // You can tell browserSync to use this directory and serve it as a mini-server
        server: {
            baseDir: "./src"
        }
        // If you are already serving your website locally using something like apache
        // You can use the proxy setting to proxy that instead
        // proxy: "yourlocal.dev"
    });
    gulp.watch("./src/sass/**/*.scss", gulp.series("sass"));
    gulp.watch("./src/*.html",reload);
});
